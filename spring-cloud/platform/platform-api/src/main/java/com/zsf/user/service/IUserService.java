package com.zsf.user.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import com.zsf.user.dto.UserDto;

import java.util.Collection;

@FeignClient(value = "user-provider")
public interface IUserService {
    /**
     * 保存用户
     *
     * @param userDto
     * @return 如果保存成功的话，返回<code>true</code>，
     * 否则返回<code>false</code>
     */
    @PostMapping(value = "/save")
    boolean save(UserDto userDto);

    /**
     * 查询所有的用户对象
     *
     * @return 不会返回<code>null</code>
     */
    @GetMapping(value = "/findAll")
    Collection<UserDto> findAll();
}
