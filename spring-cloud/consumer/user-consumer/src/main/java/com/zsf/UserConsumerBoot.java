package com.zsf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import com.zsf.user.service.IUserService;

/**
 * Hello world!
 *
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(clients = IUserService.class)

public class UserConsumerBoot
{
    public static void main(String[] args) {
        SpringApplication.run(UserConsumerBoot.class,args);
    }

}
