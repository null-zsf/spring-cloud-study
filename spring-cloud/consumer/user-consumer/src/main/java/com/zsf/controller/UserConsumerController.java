package com.zsf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.zsf.user.dto.UserDto;
import com.zsf.user.service.IUserService;

import java.util.Collection;

@RestController
public class UserConsumerController {
    @Autowired
    IUserService userService;

    @PostMapping(value = "/person/save")
    public boolean save(@RequestBody UserDto userDto) {
        return userService.save(userDto);
    }


    @GetMapping(value = "/person/find/all")
    public Collection<UserDto> findAll() {
        return userService.findAll();
    }
}
