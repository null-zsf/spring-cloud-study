package com.zsf.stream.messaging;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface MyOutput {

    /**
     * 消息发送的管道名称："zsf"
     */
    String OUTPUT1 = "output1";

    @Output(OUTPUT1)
    MessageChannel output();
}
