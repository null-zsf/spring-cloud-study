package com.zsf.stream.consumer;

import com.zsf.stream.messaging.MyInput;
import com.zsf.stream.messaging.MyMassageChannel;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Component;

@Component
@EnableBinding(MyInput.class)
public class Consumer {

    @StreamListener(MyInput.INPUT1)
    public void onMessage(String message) {
        System.out.println("@StreamListener : " + message);
    }

}
