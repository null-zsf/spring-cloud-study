package com.zsf.stream.controller;

import com.zsf.stream.producer.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StreamController {

    @Autowired
    Producer producer;

    @PostMapping("/message/send/channel")
    public Boolean sendMessage(@RequestParam String message) {
        producer.sendChannel(message);
        return true;
    }

    @PostMapping("/message/send/source")
    public Boolean sendSource(@RequestParam String message) {
        producer.sendSource(message);
        return true;
    }
}
