package com.zsf.stream.producer;

import com.zsf.stream.messaging.MyMassageChannel;
import com.zsf.stream.messaging.MyOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
@EnableBinding(MyOutput.class)
public class Producer {

    @Autowired
    private MyOutput myOutput;

    @Autowired
    @Qualifier(MyOutput.OUTPUT1) // Bean 名称
    private MessageChannel messageChannel;

    /**
     * 发送消息到 zsf
     * @param message 消息内容
     */
    public void sendChannel(String message){
        // 通过消息管道发送消息
        messageChannel.send(MessageBuilder.withPayload(message).build());
    }

    public void sendSource(String message) {
        // 通过发送源发送
        myOutput.output().send(MessageBuilder.withPayload(message).build());
    }

}
