package com.zsf.stream.messaging;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface MyInput {

    /**
     * 消息来源的通道
     */
    String INPUT1 = "input1";

    @Input(INPUT1)
    SubscribableChannel input();
}
