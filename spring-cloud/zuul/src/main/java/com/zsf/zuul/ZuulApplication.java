package com.zsf.zuul;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.refresh.ContextRefresher;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Set;

@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
public class ZuulApplication {

	@Autowired
	private ContextRefresher contextRefresher;

	public static void main(String[] args) {
		SpringApplication.run(ZuulApplication.class, args);
	}

	//定时刷新config
//	@Scheduled(fixedRate = 3000)
//	public void sch(){
//		Set<String> refresh = contextRefresher.refresh();
//		System.err.println(refresh.toString());
//	}
}
