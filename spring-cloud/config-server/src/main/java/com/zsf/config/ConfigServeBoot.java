package com.zsf.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
@EnableDiscoveryClient
public class ConfigServeBoot {

	public static void main(String[] args) {
		SpringApplication.run(ConfigServeBoot.class, args);
//        System.out.println(System.getProperty("user.home"));
    }
}
