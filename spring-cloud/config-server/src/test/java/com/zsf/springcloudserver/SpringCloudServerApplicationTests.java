package com.zsf.springcloudserver;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringCloudServerApplicationTests {

	@Test
	public void contextLoads() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getForObject("http://user-service-provider/user/list", Map.class);
	}

}
