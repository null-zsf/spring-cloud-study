package com.zsf.bo;

import com.zsf.user.dto.UserDto;

import java.util.List;

public interface IUserBo {
    /**
     * 保存用户信息
     * @param user
     * @return
     */
    public boolean save(UserDto user);

    /**
     * 获取所有用户信息
     * @return
     */
    public List<UserDto> getUsers();
}
