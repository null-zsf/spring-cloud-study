package com.zsf.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.zsf.bo.IUserBo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.zsf.user.dto.UserDto;
import com.zsf.user.service.IUserService;

import java.util.Collection;
import java.util.Collections;
import java.util.Random;

@RestController
public class UserProviderController implements IUserService{

    @Autowired
    IUserBo userBo;

    private final static Random random = new Random();

    @Override
    public boolean save(@RequestBody UserDto userDto) {
        return userBo.save(userDto);
    }


    @Override
//    @HystrixCommand(fallbackMethod = "fallbackForFindAllPersons",
//            commandProperties = {
//                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",
//                            value = "100")
//            }
//    )
    public Collection<UserDto> findAll() {

        int value = random.nextInt(200);
        System.out.println("睡眠："+value);
        try {
            Thread.sleep(value);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return userBo.getUsers();
    }


//    /**
//     * {@link #findAll()} Fallback 方法
//     *
//     * @return 返回空集合
//     */
//    public Collection<UserDto> fallbackForFindAllPersons() {
//        System.err.println("熔断保护");
//        return Collections.emptyList();
//    }
}
