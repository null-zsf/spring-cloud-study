package com.zsf.bo.Impl;

import com.zsf.bo.IUserBo;
import com.zsf.dao.model.test.User;
import com.zsf.dao.model.test.UserExample;
import com.zsf.dao.test.UserMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.zsf.user.dto.UserDto;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserBoImpl implements IUserBo{

    @Autowired
    UserMapper userMapper;


    @Override
    public boolean save(UserDto user) {
        User user1 = new User();
        BeanUtils.copyProperties(user,user1);
        return userMapper.insert(user1)==1;
    }

    @Override
    public List<UserDto> getUsers() {
        UserExample userExample = new UserExample();
        List<User> users = userMapper.selectByExample(userExample);
        List<UserDto> result = new ArrayList<>();

        for (User user:users) {
            UserDto userDto = new UserDto();
            BeanUtils.copyProperties(user,userDto);
            result.add(userDto);
        }
        return result;
    }
}
