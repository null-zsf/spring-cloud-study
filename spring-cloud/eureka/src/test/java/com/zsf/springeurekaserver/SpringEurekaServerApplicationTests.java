package com.zsf.springeurekaserver;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringEurekaServerApplicationTests {

	@Test
	public void contextLoads() {
        RestTemplate restTemplate = new RestTemplate();
        Map forObject = restTemplate.getForObject("http://windows10.microdone.cn:8080/env", Map.class);
        System.out.println(forObject);
    }

}
